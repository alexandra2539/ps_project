For my project I developed an application with mories and series information. 

The database consists in 4 tables: User, Category, Series and Movies. 

In the model package are the corresponding classes for the database tables, an interface implemented by Series class and Movies class, an enum and a factory class. 

The dao package contains a repository for each class (User, Category, Series and Movies). Each repository extend CrudRepository in order to use the implemented methods. 
In the service package are the service interfaces for each class and the implementations of the corresponding interfaces. Here are called the needed methods from the repositories. 

The last package is the controller package. It contains a controller for each class and there are used the methods from the services. 

The operations that can be done are the following: GET, POST, DELETE. They are available for each table.

The application was tested using UnitTests.

The frontend for this app is developed in android studio. 

It contains a main menu where the user must insert its username and chose one of the 3 options available. 
The first one is to start watching and it redirects the user to a page with 2 options: 
1. to see all and the user is redirected to a page with 2 tabs, one for movies and one for series where he/she can find all series and movies
2. to select a category from the list and see only the series and movies from that category.

The second option is to edit users. When selected, a new page appears where a user can be added or deleted. 

The last option is for the administraitor. There he/she cand see all series, add series, delete series, see all movies, add movies, delete movies, add categories or delete categories.

After each add or delete operation a success message appears to let the user know that the request is completed.


In this project "PS_project" you cand find the backend, on the master branch and the frontend can be found in another project "PS_project_front", also on the master branch.


Due to the fact that backend and frontend are not completely linked, the project is not fully functional and it needs to be continued.
